* sql

some notes about sql (mysql, postgresql)

** mysql

*** single point server

#+BEGIN_SRC sh
docker run --name mysql-test -e MYSQL_ROOT_PASSWORD=1234 -p3306:3306 -d mysql:5.7
#+END_SRC 

For more configuration, you can see the official documentation.

*** local mysql cluster

This is the [[file:mysql_cluster/note.org][note]] about estabilish the local mysql cluster (ndbcluster engine). You can try [[https://github.com/mattlord/Docker-InnoDB-Cluster][this one]] which use the innodb engine if you want it a try.

*** syntax 

**** create view ( from version 5.1)

You can think this is virtual table.

#+BEGIN_SRC sql

create view love_story as (
  select * from order where name = 'love'
);

-- now we can select from the view
-- ex.

select * from love_story;

#+END_SRC

**** pivot table

#+BEGIN_SRC sql

select 
        min(D),
        min(P),
        min(S),
        min(A)
    from
    (
    select  
        case when occupation = 'Doctor' then (@dn:=@dn+1)
            when occupation = 'Professor' then (@pn:=@pn+1)
            when occupation = 'Singer' then (@sn:=@sn+1)
            when occupation = 'Actor' then (@an:=@an+1)
        end as row_number,
        
        case when occupation = 'Doctor' then name end as D,
        case when occupation = 'Professor' then name end as P,
        case when occupation = 'Singer' then name end as S,
        case when occupation = 'Actor' then name end as A
    from occupations, (select @dn:=0, @pn:=0, @sn:=0, @an:=0) as temp 
    order by name asc
    ) as otemp
    group by row_number;

#+END_SRC
**** aggregation function 

You should be careful that you need to make sure other column are aggregated by aggregation function or grouped by.
